#include "ioctl_rpc.h"
#include "../master/ioctl.h"
#include <errno.h>
#include <fcntl.h>
#include <mqueue.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

static mqd_t send_mq = -1;
static mqd_t receive_mq = -1;
static char receive_mq_name[48];
static pid_t self_pid = -1;
static long mq_maxsize = 0;

#ifdef CMD_DEBUG_ON
#define CMD_DEBUG(fmt, args...) \
    printf(fmt, ##args)
#else
#define CMD_DEBUG(fmt, args...)
#endif

int init_mq()
{
    struct mq_attr attr = {0};
    self_pid = getpid();

    sprintf(receive_mq_name, MQ_NAME_FORMATE, self_pid);
    CMD_DEBUG("mq name: %s\n", receive_mq_name);
    send_mq = mq_open(MQ_MASTER_NAME, O_WRONLY, 0, NULL);
    if (send_mq == -1) {
        CMD_DEBUG("open master mq fail %d, %s\n", errno, strerror(errno));
        return 0;
    }

    receive_mq = mq_open(receive_mq_name, O_CREAT | O_EXCL | O_RDONLY, S_IRUSR | S_IWUSR, NULL);
    if (receive_mq == -1 && errno == EEXIST) {
        CMD_DEBUG("unlink and create\n");
        mq_unlink(receive_mq_name);
        receive_mq = mq_open(receive_mq_name, O_CREAT | O_EXCL | O_RDONLY, S_IRUSR | S_IWUSR, NULL);
    }
    if (receive_mq == -1) {
        CMD_DEBUG("open slave mq fail %d, %s\n", errno, strerror(errno));
        mq_close(send_mq);
        return 0;
    }

    mq_getattr(receive_mq, &attr);
    CMD_DEBUG("max msg size: %ld, max msg num: %ld\n", attr.mq_msgsize, attr.mq_maxmsg);
    mq_maxsize = attr.mq_msgsize;
    return 0;
}

void release_mq()
{
    CMD_DEBUG("release mq\n");
    if (send_mq != -1) {
        mq_close(send_mq);
    }

    if (receive_mq != -1) {
        mq_close(receive_mq);
        mq_unlink(receive_mq_name);
    }
    return;
}

static int send_req_and_wait_resp(cmd_base_req_t *rpc_request, cmd_base_resp_t *rpc_respond, size_t payload_size,
    unsigned long func_id, unsigned long timeout_sec)
{
    struct timespec timeout_time;
    clock_gettime(CLOCK_REALTIME, &timeout_time);
    timeout_time.tv_sec += timeout_sec;
    if (!rpc_request || !rpc_respond) {
        CMD_DEBUG("[ERROR] null input");
        return -1;
    }

    int ret = mq_timedsend(send_mq, (char *)rpc_request, payload_size, 0, &timeout_time);
    if (ret == -1) {
        CMD_DEBUG("send msg fail, %s\n", strerror(errno));
        return -1;
    }

    rpc_respond->func_id = 0;
    while (rpc_respond->func_id != func_id) {
        CMD_DEBUG("wait for msg, prev func_id:%lu...\n", rpc_respond->func_id);
        ret = mq_timedreceive(receive_mq, (char *)rpc_respond, mq_maxsize, NULL, &timeout_time);
        if (ret == -1) {
            CMD_DEBUG("receive msg fail, %s\n", strerror(errno));
            return -1;
        }
    }
    return 0;
}

static inline int rpc_malloc(void **rpc_request, void **rpc_respond, size_t payload_size)
{
    *rpc_request = malloc(payload_size);
    if (!(*rpc_request)) {
        return -1;
    }
    *rpc_respond = malloc(mq_maxsize);
    if (!(*rpc_respond)) {
        free(*rpc_request);
        return -1;
    }
    return 0;
}

static inline void rpc_free(void *rpc_request, void *rpc_respond)
{
    if (rpc_request != NULL) free(rpc_request);
    if (rpc_respond != NULL) free(rpc_respond);
}

static int inline is_special_request(unsigned int request)
{
    return (
        request == EC_IOCTL_SLAVE_SDO_UPLOAD ||
        request == EC_IOCTL_SLAVE_SDO_DOWNLOAD ||
        request == EC_IOCTL_SLAVE_SII_READ ||
        request == EC_IOCTL_SLAVE_SII_WRITE ||
        request == EC_IOCTL_SLAVE_REG_READ ||
        request == EC_IOCTL_SLAVE_REG_WRITE ||
        request == EC_IOCTL_SLAVE_FOE_READ ||
        request == EC_IOCTL_SLAVE_FOE_WRITE ||
        request == EC_IOCTL_SLAVE_SOE_READ ||
        request == EC_IOCTL_SLAVE_SOE_WRITE
        );
}

static void get_extra_mem(unsigned int request, void *arg, void** ptr, size_t* size)
{
    *ptr = NULL;
    *size = 0;
    switch (request)
    {
        case EC_IOCTL_SLAVE_SDO_UPLOAD:
            *size = ((ec_ioctl_slave_sdo_upload_t *)arg)->target_size;
            *ptr = ((ec_ioctl_slave_sdo_upload_t *)arg)->target;
            return;
        case EC_IOCTL_SLAVE_SDO_DOWNLOAD:
            *size = ((ec_ioctl_slave_sdo_download_t *)arg)->data_size;
            *ptr = ((ec_ioctl_slave_sdo_download_t *)arg)->data;
            return;
        case EC_IOCTL_SLAVE_SII_READ:
        case EC_IOCTL_SLAVE_SII_WRITE:
            *size = (((ec_ioctl_slave_sii_t *)arg)->nwords * sizeof(uint16_t));
            *ptr = ((ec_ioctl_slave_sii_t *)arg)->words;
            return;
        case EC_IOCTL_SLAVE_REG_READ:
        case EC_IOCTL_SLAVE_REG_WRITE:
            *size = ((ec_ioctl_slave_reg_t *)arg)->size;
            *ptr = ((ec_ioctl_slave_reg_t *)arg)->data;
            return;
        case EC_IOCTL_SLAVE_FOE_READ:
        case EC_IOCTL_SLAVE_FOE_WRITE:
            *size = ((ec_ioctl_slave_foe_t *)arg)->buffer_size;
            *ptr = ((ec_ioctl_slave_foe_t *)arg)->buffer;
            return;
        case EC_IOCTL_SLAVE_SOE_READ:
            *size = ((ec_ioctl_slave_soe_read_t *)arg)->mem_size;
            *ptr = ((ec_ioctl_slave_soe_read_t *)arg)->data;
            return;
        case EC_IOCTL_SLAVE_SOE_WRITE:
            *size = ((ec_ioctl_slave_soe_write_t *)arg)->data_size;
            *ptr = ((ec_ioctl_slave_soe_write_t *)arg)->data;
            return;
    }
    return;
}

void set_extra_mem_ptr(unsigned int request, void *arg, void* ptr)
{
    switch (request)
    {
        case EC_IOCTL_SLAVE_SDO_UPLOAD:
            ((ec_ioctl_slave_sdo_upload_t *)arg)->target = (uint8_t *)ptr;
            return;
        case EC_IOCTL_SLAVE_SDO_DOWNLOAD:
            ((ec_ioctl_slave_sdo_download_t *)arg)->data = (uint8_t *)ptr;
            return;
        case EC_IOCTL_SLAVE_SII_READ:
        case EC_IOCTL_SLAVE_SII_WRITE:
            ((ec_ioctl_slave_sii_t *)arg)->words = (uint16_t *)ptr;
            return;
        case EC_IOCTL_SLAVE_REG_READ:
        case EC_IOCTL_SLAVE_REG_WRITE:
            ((ec_ioctl_slave_reg_t *)arg)->data = (uint8_t *)ptr;
            return;
        case EC_IOCTL_SLAVE_FOE_READ:
        case EC_IOCTL_SLAVE_FOE_WRITE:
            ((ec_ioctl_slave_foe_t *)arg)->buffer = (uint8_t *)ptr;
            return;
        case EC_IOCTL_SLAVE_SOE_READ:
            ((ec_ioctl_slave_soe_read_t *)arg)->data = (uint8_t *)ptr;
            return;
        case EC_IOCTL_SLAVE_SOE_WRITE:
            ((ec_ioctl_slave_soe_write_t *)arg)->data = (uint8_t *)ptr;
            return;
    }
    return;
}

// 替代 ioctl, eccdev_ioctl
int stub_ioctl(int fd, int request, ...)
{
    void *arg;
    int ret = 0;
    cmd_ioctl_req_t *rpc_request = NULL;
    cmd_ioctl_resp_t *rpc_respond = NULL;
    size_t extra_mem_size = 0;
    void* extra_mem_ptr = NULL;
    int is_special = is_special_request(request);

    CMD_DEBUG("enter ioctl\n");

    va_list ap;
    va_start(ap, request);
    arg = va_arg(ap, void *);
    va_end(ap);

    // 获取master，默认只有一个master，不管fd是多少
    size_t arg_size = _IOC_SIZE(request);
    size_t payload_size = arg_size + sizeof(cmd_ioctl_req_t);
    if (arg_size == 0) {
        payload_size += sizeof(void *);
    } else if (is_special) {
        get_extra_mem(request, arg, &extra_mem_ptr, &extra_mem_size);
        payload_size += extra_mem_size;
    }

    ret = rpc_malloc((void **)&rpc_request, (void **)&rpc_respond, payload_size);
    if (ret) {
        CMD_DEBUG("malloc fail\n");
        return ret;
    }

    rpc_request->func_id = IGH_IOCTL_ID;
    rpc_request->pid = self_pid;
    rpc_request->request = (unsigned int)request;
    rpc_request->arg_size = (int)(arg_size + extra_mem_size);
    rpc_request->fd = fd;
    if (arg_size == 0) {
        CMD_DEBUG("no arg, param:%lu\n", arg);
        memcpy(rpc_request->arg, &arg, sizeof(void *));
    } else {
        memcpy(rpc_request->arg, arg, arg_size);
        if (is_special) {
            memcpy(&(rpc_request->arg[arg_size]), extra_mem_ptr, extra_mem_size);
        }
    }

    CMD_DEBUG("send request: func_id: %lu, arg_size: %d, request: %u, fd:%d\n",
        rpc_request->func_id, rpc_request->arg_size, rpc_request->request, fd);
    ret = send_req_and_wait_resp((cmd_base_req_t *)rpc_request, (cmd_base_resp_t *)rpc_respond, payload_size, IGH_IOCTL_ID, 10);
    if (ret == 0) {
        ret = rpc_respond->ret;
        errno = rpc_respond->remote_errno;
        if (ret >= 0 && arg_size != 0) {
            CMD_DEBUG("recevie respond: func_id: %lu, arg_size %d, ret: %d\n",
                rpc_respond->func_id, rpc_respond->arg_size, rpc_respond->ret);
            memcpy(arg, rpc_respond->arg, arg_size);
            if (is_special) {
                memcpy(extra_mem_ptr, &(rpc_respond->arg[arg_size]), extra_mem_size);
                set_extra_mem_ptr(request, arg, extra_mem_ptr);
            }
        }
    }
    rpc_free(rpc_request, rpc_respond);
    return ret;
}

int stub_open() {
    init_mq();

    int ret = 0;
    cmd_base_req_t *rpc_request = NULL;
    cmd_base_resp_t *rpc_respond = NULL;

    CMD_DEBUG("enter open\n");

    size_t payload_size = sizeof(cmd_base_req_t);
    ret = rpc_malloc((void **)&rpc_request, (void **)&rpc_respond, payload_size);
    if (ret) {
        CMD_DEBUG("malloc fail\n");
        return ret;
    }
    rpc_request->func_id = IGH_OPEN_ID;
    rpc_request->pid = self_pid;

    CMD_DEBUG("send request: func_id: %lu\n", rpc_request->func_id);
    ret = send_req_and_wait_resp((cmd_base_req_t *)rpc_request, (cmd_base_resp_t *)rpc_respond, payload_size, IGH_OPEN_ID, 10);
    if (ret == 0) {
        CMD_DEBUG("recevie respond: func_id: %lu, ret: %d\n", rpc_respond->func_id, rpc_respond->ret);
        ret = rpc_respond->ret;
        errno = rpc_respond->remote_errno;
    }

    rpc_free(rpc_request, rpc_respond);
    // TODO 换成 ret
    return ret;
}

void stub_close(int fd) {
    int ret = 0;
    cmd_close_req_t *rpc_request = NULL;
    cmd_base_resp_t *rpc_respond = NULL;

    CMD_DEBUG("enter close\n");

    size_t payload_size = sizeof(cmd_close_req_t);
    ret = rpc_malloc((void **)&rpc_request, (void **)&rpc_respond, payload_size);
    if (ret) {
        CMD_DEBUG("malloc fail\n");
        return;
    }
    rpc_request->func_id = IGH_CLOSE_ID;
    rpc_request->pid = self_pid;
    rpc_request->fd = fd;

    CMD_DEBUG("send request: func_id: %lu\n", rpc_request->func_id);
    ret = send_req_and_wait_resp((cmd_base_req_t *)rpc_request, (cmd_base_resp_t *)rpc_respond, payload_size, IGH_CLOSE_ID, 10);
    if (ret == 0) {
        CMD_DEBUG("recevie respond\n");
        if (rpc_respond->ret < 0) {
            CMD_DEBUG("[ERROR] close msg send fail!, %s\n", strerror(rpc_respond->remote_errno));
        }
    }

    rpc_free(rpc_request, rpc_respond);
    release_mq();
    return;
}
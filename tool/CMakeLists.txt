# 根据 Kbuild.in, ec_master-objs 选择源文件
set(IGH_MASTER_SRC
    ioctl_rpc.c
    Command.cpp
    CommandAlias.cpp
    CommandCrc.cpp
    CommandCStruct.cpp
    CommandConfig.cpp
    CommandData.cpp
    CommandDebug.cpp
    CommandDomains.cpp
    CommandDownload.cpp
    CommandFoeRead.cpp
    CommandFoeWrite.cpp
    CommandGraph.cpp
    CommandMaster.cpp
    CommandPdos.cpp
    CommandRegRead.cpp
    CommandRegWrite.cpp
    CommandRescan.cpp
    CommandSdos.cpp
    CommandSiiRead.cpp
    CommandSiiWrite.cpp
    CommandSlaves.cpp
    CommandSoeRead.cpp
    CommandSoeWrite.cpp
    CommandStates.cpp
    CommandUpload.cpp
    CommandVersion.cpp
    CommandXml.cpp
    DataTypeHandler.cpp
    FoeCommand.cpp
    MasterDevice.cpp
    NumberListParser.cpp
    SdoCommand.cpp
    SoeCommand.cpp
    main.cpp
    sii_crc.cpp
)


add_library(ethercat_obj OBJECT ${IGH_MASTER_SRC})

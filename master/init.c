#include <stdio.h>
#define MAX_MASTERS 32 /**< Maximum number of masters. */

int ec_gen_init_module(void);
void ec_gen_cleanup_module(void);
int ec_init_module(void);
void ec_cleanup_module(void);

static int ethercat_init_success = 0;

static char main[] = "ff:ff:ff:ff:ff:ff";

static char *main_devices[MAX_MASTERS]; /**< Main devices parameter. */
// static unsigned int master_count; /**< Number of masters. */
// static char *backup_devices[MAX_MASTERS]; /**< Backup devices parameter. */
// static unsigned int backup_count; /**< Number of backup devices. */
// static unsigned int debug_level;  /**< Debug level parameter. */

int ec_set_module_param(char **main_devices_param, unsigned int master_count_param, char **backup_devices_param,
    unsigned int backup_count_param, unsigned int debug_level_param);

int ethercat_init()
{
    int ret;
    main_devices[0] = main;
    printf("[TEST] enter init\n");
    ret = ec_set_module_param(main_devices, 1, 0, 0, 1);
    if (ret) return ret;
    printf("[TEST] init module\n");
    ret = ec_init_module();
    if (ret) return ret;
    printf("[TEST] init gen\n");
    ret = ec_gen_init_module();
    if (ret) {
        printf("[TEST] init fail\n");
        ec_cleanup_module();
        return ret;
    }
    ethercat_init_success = 1;
    printf("[TEST] init success\n");
    return 0;
}

void ethercat_exit()
{
    ec_cleanup_module();
    ec_gen_cleanup_module();
    ethercat_init_success = 0;
}

int is_ethercat_int()
{
    return ethercat_init_success;
}